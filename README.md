# 我叫罗米奇-TaoliSystem+


## 系统介绍

![陶丽系统概览](ReadMeFile/welcome_TaoLiSystem_gif.gif)

这里是罗米奇所开发的陶丽系统（TaoliSystem），陶丽系统是做为MicroPython嵌入式系统的典型。

此仓库是陶丽系统的优化增强总集，本人其原作者“我叫以赏”的工作成果表示高度的赞同与认可，但陶丽系统还有美中不足之处，因此，我对此系统进行了优化、修复、开发、拓展的任务。如果您是第一次来到这个仓库，且对此系统不了解，请您参考原网址，在此网址当中会告诉您桃李系统的更多详细信息 -> [系统原地址-我叫以赏](https://gitee.com/wojiaoyishang/TaoLiSystem)

### 系统适配的设备
此系统必须要在**掌控板或Matrix:bit开发板**（Matrix:bit开发板同掌控板）中运行。掌控板是一块MicroPython微控制器板，具有良好的支持于mPythonX/MicroPython/Python软件上功能。大小仅为信用卡的一半，板载有加速度计、按键、触摸引脚、声光传感器以及128*64的OLED屏幕，主要用于青少年的编程教育。利用掌控板，你可以实现任何酷炫的小发明，无论是机器人还是乐器，其乐无穷。

## 系统信息
### 系统版本
此系统一共有两个主要的版本：
第一个版本是V1版本
第二个版本是V2版本
您可以根据您自己的实际情况需要以选择合适的操作系统版本
>**TaolisysytemV1+**

>**TaolisysytemV2+**

### 系统架构
桃丽系统运行时会调用多个自带的代码文件，以确保系统正常运行与工作，作者所发布的文件结构与其类似，下面将阐述各个版本的其目录结构。

#### 陶丽系统V1+，系统文件架构

```
TaoLiSystem
│  boot.py                  # 掌控板启动一定会运行的文件，非必要，这里面设计了开机动画
│  main.py                  # 桃丽系统主要引导文件，从这里开始会调用 TaoLiSystem 文件夹内的文件
│  tips.py                  # 没什么用的文件，用于开发者开发时记录的“记事本”
└─TaoLiSystem               # 桃丽系统的核心文件会放在 TaoLiSystem 中
    │  config.json          # 桃丽系统的配置文件
    │  config.py            # 桃丽系统调用配置文件的模块
    │  function.py          # 桃丽系统整合的使用函数的模块
    │  image.py             # 桃丽系统存储小图片的模块
    │  ItemSelector.py      # 桃丽系统的物品选择器模块
    │  loader.py            # 桃丽系统的初始化加载
    │  morseType.py         # 桃丽系统的摩尔斯电码输入模块
    │  TXTreader.py         # 桃丽系统的文本阅读模块
    │  wifi.py              # 桃丽系统用于控制wifi的模块
    ├─font                  # 桃丽系统的调用的字体所放的目录
    │      arlrdbd.py       # Arial Rounded MT Bold 字体 (个人使用，不得商用)
    │      HYShiGuangTiJ.py # 汉仪时光体简 Regular 字体 (个人使用，不得商用)
    ├─page                  # 桃丽系统页面存放目录
    │      home.py          # 桃丽系统主页源码
    │      program.py       # 桃丽系统程序页源码
    │      setting.py       # 桃丽系统设置页源码
    ├─picture               # 桃丽系统图片放置目录
    │      loadingPage.bmp  # 加载页面图片
    │      waitingPage.bmp  # 等待页面图片
    └─program               # 程序存放目录
```

#### 陶丽系统V2+，系统文件架构

```
掌控板根目录
TaoliSystem
│  boot.py  # 启动文件，内有 BootLoader 模式。
│  main.py  # 主要代码文件，用于分配显示的页面与处理系统逻辑
│
└─TaoLiSystem  # 桃丽系统的代码文件夹
        │  SYSVERSION  # 系统版本文件
        │  COPYRIGHT  # 版权文件，用于系统关于
        │
        ├─core  # 系统核心文件夹
        │      config.py  # 系统配置文件读取工具、全局变量存放位置
        │      sysgui.py  # 系统 GUI 绘制，所有常见 GUI 界面都在这里
        │      utils.py  # 系统零碎的代码集合
        │
        ├─data  # 数据存放文件夹
        │      config.ini  # 系统配置文件
        │
        ├─modules  # 系统外接的模块文件夹存放位置
        ├─page  # 系统的主要页面
        │      home.py  # 主页面
        │      homeFun.py  # 主页面的小功能
        │      program.py  # 程序页面
        │      setting.py  # 设置页面
        │      settingFun.py  # 详细设置项
        │
        ├─programs  # 文件文件夹
        │  └─HelloWorld  # 示例程序文件夹
        │         ico.bin  # 插件图标、图片存放文件
        │         __init__.json  # 程序信息文件
        │         __init__.py  # 程序启动代码
        │
        └─static  # 资源文件存放位置
```


## 安装教程
系统的安装方法请查看 -> [系统安装指南-我叫以赏](https://wojiaoyishang.gitee.io/taolisystem-doc/welcome/quickstart.html)

### 更新日志

#### 陶丽系统V1+系统更新日志

>TaoliSystem 1.0.0-1.0.6
<br>&emsp;&emsp;优化系统，小改了系统的设置与架构，加入了“使用说明”项，便于用户查阅使用教程

>TaoliSystem 1.0.7
<br>&emsp;&emsp;修复了系统的ntptime（自动对时）部分Bug

#### 陶丽系统V2+系统更新日志

>TaoliSystem 2.0.1
<br>&emsp;&emsp;修复了系统熄屏异常的Bug

>TaoliSystem 2.0.2-2.0.4
<br>&emsp;&emsp;更改了系统的显示样式，使其更加的适合于系统风格
<br>&emsp;&emsp;增加了本人2个自制程序
<br>&emsp;&emsp;修复了部分Bug

>TaoliSystem 2.0.5-2.0.6
<br>&emsp;&emsp;增加了系统的蓝牙显示MAC地址功能，同时修复了在文本输入界面在按下O键时没有反应的问题
<br>&emsp;&emsp;更改了系统的部分显示界面，以及本人2个自制程序的提示

>TaoliSystem 2.0.7-2.0.8
<br>&emsp;&emsp;修复了部分的系统的Bug，由插件改为了程序
<br>&emsp;&emsp;修复了熄屏时间显示的异常Bug

>TaoliSystem 2.0.9
<br>&emsp;&emsp;更改了设置的Wifi的选择项的显示顺序
<br>&emsp;&emsp;修复了MiniOS程序进入卡死的Bug

>TaoliSystem 2.1.0
<br>&emsp;&emsp;更改了设置的蓝牙信息的显示
<br>&emsp;&emsp;更改了MiniOS程序的显示界面

## 参与贡献与其它
系统开发采用 MulanPSL-2.0 许可证。同时，此系统不可用于商业活动，仅作为个人和公益使用，严禁随意以不正当、不明确理由贩卖。任何以此项目衍生的项目需要尽可能一并开源。
<br>贡献人员：
1.  我叫以赏（原作者）
2.  罗米奇（开发/优化）

本仓库的作者：罗米奇
首次发布时间：2023年11月11日，10点40分